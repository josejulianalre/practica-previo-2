const data = fetch("https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/json/estudiantes.json").then((response) => response.json());

const autoevaluacion = async () => {
    codigo = document.querySelector("#codigo").value;
    eliminar = document.querySelector("#eliminar").value;
    datos = await data;
    estudiante = datos["estudiantes"].find(estudiante => estudiante.codigo == codigo);
    if(estudiante != null) {
        let promedio = await eliminadasAprovadasReprobadas(estudiante,eliminar,3);
        alert(`Mi nota es de : ${promedio} \nEsta nota es debido a: Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum veritatis tempora, atque aliquam molestiae reprehenderit rerum libero, rem, assumenda nulla harum. Esse sunt ut doloremque.`);
    }
    else {
        alert("Ingrese el código del estudiante a consultar / Ese estudiante no esta registrado");
    }
};

const cargarNotas = async () => {
    let datos = await data;
    let url = new URLSearchParams(location.search);
    let codigo = Number(url.get("codigo"));
    let eliminar = Number(url.get("eliminar"));
    let estudiante = datos["estudiantes"].find(estudiante => estudiante.codigo == codigo);

    if (estudiante == null) {
        mensajeError();
    } else {
        graficoDatos(estudiante);
        graficoNotas(estudiante, eliminar);
        graficoAR(estudiante,eliminar);
    }
}

const graficoDatos = (estudiante) => {
    var data = new google.visualization.DataTable();
    data.addColumn('string');
    data.addColumn('string');
    data.addRows([
        ['Codigo', (estudiante.codigo).toString()],
        ['nombre', estudiante.nombre],
        ['materia', 'Programación Web'],
    ]);
    var table = new google.visualization.Table(document.getElementById('descripcion'));
    table.draw(data, { showRowNumber: false, width: '100%', height: '100%' });
}

const graficoNotas = async (notas, notasE) => {
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Descripcion');
    data.addColumn('string', 'valor');
    data.addColumn('string', 'observacion');
    let datos = await eliminadasAprovadasReprobadas(notas,notasE,1);
    for(let v of datos){
        data.addRows([v]);
    }
    
    var table = new google.visualization.Table(document.getElementById('calificaciones'));
    table.draw(data, { showRowNumber: true, width: '100%', height: '100%' });
}

const graficoAR = async (estudiante,eliminar)=>{
    let aR = await eliminadasAprovadasReprobadas(estudiante,eliminar,2);

    var data = google.visualization.arrayToDataTable([
        ['Aprobadas','cantidad'],
        ['Aprobadas',aR[0]],
        ['Desaprobadas',aR[1]],
      ]);

      var chart = new google.visualization.PieChart(document.getElementById('grafico'));
      chart.draw(data);
}

const mensajeError = ()=>{
    container = document.querySelector('#descripcion');
    let contentHTML = ` <h1 class="text-center">ERROR</h1>
    <p class="text-center">El codigo del estudiante no se ha encontrado, intentalo con otro</p>
`;
    container.innerHTML = contentHTML;
}

const eliminadasAprovadasReprobadas = async (estudiante, eliminas, tipo) => {
    let notas = estudiante.notas;
    let sumN = 0;
    let descripciones = await data;
    let descripcion;
    let salida = [];
    let aprov = 0;
    let reprov = 0;

    notas.sort((a,b)=>{
        if(a.valor < b.valor){
            return -1;
        }
        if(a.valor > b.valor){
            return 1;
        }
        return 0;
    });

    for(let i = 0; i < eliminas;i++){
        descripcion = descripciones["descripcion"].find(des => des.id == notas[i].id);
        salida.push([descripcion.descripcion, (notas[i].valor).toString(), "Nota Eliminada"]);
    }
    for(let i = eliminas; i < notas.length;i++){
        descripcion = descripciones["descripcion"].find(des => des.id == notas[i].id);
        if (parseFloat(notas[i].valor) >= 3.0) {
            salida.push([descripcion.descripcion, (notas[i].valor).toString(),"Nota Aprobada"]);
            aprov++;
        } else {
            salida.push([descripcion.descripcion, (notas[i].valor).toString(),"Nota Reprovada"]);
            reprov++;
        }
        sumN += parseFloat(notas[i].valor);
    }

    if (sumN != 0) {
        salida.push(['NOTA DEFINITIVA', ((sumN / (notas.length-eliminas)).toFixed(1)).toString(), 'NOTA DEFINITIVA']);

    } else {
        salida.push(['NOTA DEFINITIVA', '0.0', 'NOTA DEFINITIVA']);
    }

    if (tipo == 1) {
        return salida;
    }
    if(tipo == 2){
        return [aprov,reprov];
    }
    return (sumN / (notas.length-eliminas)).toFixed(1);
   
}
